using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEditor.Rendering;
using UnityEngine;

public class Looper : MonoBehaviour
{
    [SerializeField] List<Transform> points;
    [SerializeField] bool looped;
    [SerializeField] Transform movingObject;
    [SerializeField] float speed;

    int i;
    int j;
    private void Start()
    {
        i = 0;
        j = 0;
        movingObject.position = points[i].position;
    }
    void LateUpdate()
    {
        if(looped)
        {
            if(Vector3.Distance(movingObject.position, points[i].position) <0.01f)
            {
                i++;
                if (i == points.Count)
                {
                    i = 0;
                    movingObject.position = points[i].position;
                }
            }
        }
        else
        {
            if (Vector3.Distance(movingObject.position, points[i].position) < 0.01f)
            { 
                j++;
                i = Utility.PingPong(j, points.Count-1);    
            }

            
        }
        movingObject.position = Vector3.MoveTowards(movingObject.position, points[i].position, Time.deltaTime * speed);
    }
}
