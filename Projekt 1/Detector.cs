using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Detector : MonoBehaviour
{
    bool isActive = false;
    bool wasActive = false;

    [SerializeField] UnityEvent OnDetectorActivated;
    [SerializeField] UnityEvent OnDetectorDeactivated;

    // Update is called once per frame
    void Update()
    {
        if(isActive)
        {
            if (!wasActive)
            {
                OnDetectorActivated?.Invoke();
                wasActive = true;
            }
        }
        if(!isActive)
        {
            if(wasActive)
            {
                OnDetectorDeactivated?.Invoke();
                wasActive = false;
            }
        }
    }

    public void ChangeValue(bool isDetectorHit)
    {
        isActive = isDetectorHit;
    }
}
