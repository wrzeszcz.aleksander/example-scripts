using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class LightEmiter : MonoBehaviour
{
    LineRenderer lineRenderer;
    [SerializeField] Transform start;
    [SerializeField] int maxReflections;
    [SerializeField] float maxLength;
    [SerializeField] Detector detector;
    bool isDetectorHit;
    

    void Start()
    {
        lineRenderer = GetComponent<LineRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        CastLaser(start.position, transform.forward);
        HandleDetector();
    }

    private void CastLaser(Vector3 position, Vector3 direction)
    {
        Ray ray = new Ray(position, direction);

        isDetectorHit = false;

        lineRenderer.positionCount = 1;
        lineRenderer.SetPosition(0, start.position);
        float remaningLength = maxLength;
        for (int i = 0; i < maxReflections; i++)
        {
            
            RaycastHit hit;

            
            if(Physics.Raycast(ray, out hit, remaningLength))
            {
                Debug.Log(hit.transform.name);
                lineRenderer.positionCount++;
                lineRenderer.SetPosition(lineRenderer.positionCount-1, hit.point);
                remaningLength -= Vector3.Distance(ray.origin, hit.point);
                ray = new Ray(hit.point, Vector3.Reflect(direction, hit.normal));

                if (hit.transform.CompareTag("Detector"))
                {
                    isDetectorHit = true;
                    detector = hit.transform.GetComponent<Detector>();
                }
                if (!hit.transform.CompareTag("Mirror")) 
                    break;
            }
            else
            {
                lineRenderer.positionCount++;
                lineRenderer.SetPosition(lineRenderer.positionCount - 1, start.position + direction * remaningLength);
            }
        }
    }

    private void HandleDetector()
    { 
        detector.ChangeValue(isDetectorHit);
    }
}
