using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PressurePlate : MonoBehaviour
{
    [SerializeField] Vector3 pressVector;
    [SerializeField] float pressureSpeed;
    [SerializeField] UnityEvent OnPlatePressed;
    [SerializeField] UnityEvent OnPlateUnPressed;

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Interactable") || other.CompareTag("Player"))
        {
            Interact();
            StartCoroutine(PressPlate(pressVector));
        }
    }
    private void Interact()
    {
        OnPlatePressed?.Invoke();
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Interactable") || other.CompareTag("Player"))
        {
            Uninteract();
            StartCoroutine(PressPlate(-pressVector));
        }
    }

    private void Uninteract()
    {
        OnPlateUnPressed?.Invoke();
    }

    private IEnumerator PressPlate(Vector3 moveVector)
    {
        Vector3 target = transform.position + moveVector;
        while (target != transform.position)
        {
            transform.position = Vector3.MoveTowards(transform.position, target, pressureSpeed * Time.deltaTime);
            yield return new WaitForEndOfFrame();
        }
    }

}
