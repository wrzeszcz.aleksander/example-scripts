using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Events;

public class Lever : MonoBehaviour
{
    [SerializeField] GameObject handle;
    [SerializeField] Vector3 rotationValue = new Vector3(0, 0, 90); 
    [SerializeField] float rotationSpeed;
    [SerializeField] UnityEvent OnLeverActivated;
    [SerializeField] UnityEvent OnLeverDeactivated;
    bool isRotating = false;

    bool active;
    Outline outline;
    private void Awake()
    {
        outline = GetComponent<Outline>();
        outline.enabled = false;
        active = false;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
           outline.enabled = true;
           InputReaderSO.OnInteractEvent += OnInteract;
        }
    }

    private void OnInteract()
    {
        if(!isRotating)
        StartCoroutine(RotateLever());
    }

    private IEnumerator RotateLever()
    {
        isRotating = true;
        Vector3 targetEulerAngle = handle.transform.localEulerAngles + rotationValue;
        targetEulerAngle.x %= 360f;
        targetEulerAngle.y %= 360f;
        targetEulerAngle.z %= 360f;

        while (Mathf.Abs((handle.transform.localEulerAngles - targetEulerAngle).magnitude %360f) > 1)
        {
            handle.transform.Rotate(rotationValue * rotationSpeed);
            yield return new WaitForEndOfFrame();
        }
        rotationValue *= -1f;
        isRotating=false;
        active = !active;
        if (active) OnLeverActivated.Invoke();
        if(!active) OnLeverDeactivated.Invoke();
        yield return null;
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            outline.enabled = false;
            InputReaderSO.OnInteractEvent -= OnInteract;
        }
    }

}
