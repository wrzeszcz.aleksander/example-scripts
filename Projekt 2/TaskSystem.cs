using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TaskSystem : MonoBehaviour
{
    [SerializeField] GameObject workerPrefab;

    List<ProductionBuilding> productions;
    List<Warehouse> warehouses;

    TaskManager taskManager;

    private void Awake()
    {
        taskManager = new TaskManager();

        productions = new List<ProductionBuilding>();
        warehouses = new List<Warehouse>();
        
    }
    void Start()
    {
        CreateWorker();
    }

    private void OnEnable()
    {
        ProductionBuilding.OnProductionBuild += AddProductionBuilding;
        Warehouse.OnWarehouseBuild += AddWarehouseBuilding;

        ExtractionBuilding.OnExtraction += CreateTask;
        ProductionBuilding.OnResourceProduced += CreateTask;
    }

    public void CreateWorker()
    {
        GameObject worker = Instantiate(workerPrefab, Vector3.zero, Quaternion.identity);
        worker.GetComponent<Worker>().Setup(taskManager);
    }

    private void CreateTask(ExtractionBuilding building)
    {
        Task.Transport task = new Task.Transport();
        task.startingPlace = building.transform;
        task.resourceSO = building.resourceSO;
        if (productions.Count > 0)
            task.endingPlace = FindClosestProducer(building).transform;
        else return;
        taskManager.AddTask(task);
    }
    private void CreateTask(ProductionBuilding building)
    {
        Task.Transport task = new Task.Transport();
        task.startingPlace = building.transform;
        task.resourceSO = building.outputResourceSO;
        if (warehouses.Count > 0)
            task.endingPlace = FindClosestWarehouse(building).transform;
        else return;
        taskManager.AddTaskAtFront(task);
    }

    private Warehouse FindClosestWarehouse(ProductionBuilding building)
    {
        Warehouse candidate = null;
        float distance = Mathf.Infinity;
        foreach (var warehouse in warehouses)
        {
            if ((warehouse.transform.position - building.transform.position).magnitude < distance || candidate == null)
            {
                candidate = warehouse;
            }
        }
        return candidate;
    }

    private ProductionBuilding FindClosestProducer(ExtractionBuilding building)
    {
        ProductionBuilding candidate = null;
        float distance = Mathf.Infinity;
        foreach (var production in productions)
        {
            if((production.transform.position - building.transform.position).magnitude < distance || candidate == null) 
            {
                candidate = production;
            }
        }
        return candidate;
    }

    private void AddWarehouseBuilding(Warehouse warehouse)
    {
        warehouses.Add(warehouse);
    }

    private void AddProductionBuilding(ProductionBuilding building)
    {
       productions.Add(building);
    }


}
