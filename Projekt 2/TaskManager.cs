using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TaskManager
{
    List<Task> tasks;

    public TaskManager()
    {
        tasks = new List<Task>();
    }

    public Task RequestTask()
    {
        if (tasks.Count > 0)
        {
            Task task = tasks[0];
            tasks.RemoveAt(0);
            return task;
        }
        else 
        { 
            return null; 
        }
    }

    public void AddTask(Task task)
    {
        tasks.Add(task);
    }

    public void AddTaskAtFront(Task task)
    {
        tasks.Insert(0, task);
    }
}