using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Unity.VisualScripting;
using UnityEditor;
using UnityEditor.Animations;
using UnityEngine;
using UnityEngine.AI;

public class Worker : MonoBehaviour
{
    private enum State
    {
        Wait,
        Execute,
    }

    [SerializeField] GameObject box;
    [SerializeField][Tooltip("Frequency(1/[s]) of asking for a task(can be performance heavy)")] float askForTaskInterval;

    State state;
    Animator animator;
    GameResourcesList resources;
    float timer;
    Task actualTask;
    TaskManager taskManager;
    NavMeshAgent navMeshAgent;

    public void Setup(TaskManager manager)
    {
        animator = GetComponentInChildren<Animator>();
        resources = GetComponent<GameResourcesList>();
        navMeshAgent = GetComponent<NavMeshAgent>();
        timer = askForTaskInterval;
        state = State.Wait;
        taskManager = manager;
        box.SetActive(false);
    }

    private void Update()
    {
        switch (state)
        {
            case State.Wait:
                timer -=Time.deltaTime;
                if(timer <= 0)
                {
                    timer = askForTaskInterval;
                    RequestTask();
                }
                break;
            case State.Execute:
                break;
            default:
                break;
        }
    }

    private void RequestTask()
    {
        actualTask = taskManager.RequestTask();
        if(actualTask == null)
        {
            state = State.Wait;
        }
        else
        {
            state = State.Execute;
            switch (actualTask)
            {
                case Task.Transport:
                    navMeshAgent.destination = (actualTask as Task.Transport).startingPlace.position;
                    animator.SetBool("IsWalking", true);
                    (actualTask as Task.Transport).startingPlace.GetComponent<TaskTriggerManager>().SetTriggerReadyToTask(true);
                    break;

                //Add more task types here and handling methods below
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (actualTask == null) return;
        if(other.transform.position == (actualTask as Task.Transport).startingPlace.position)
        {
            other.GetComponent<GameResourcesList>().TryUse((actualTask as Task.Transport).resourceSO, 1);
            (actualTask as Task.Transport).startingPlace.GetComponent<TaskTriggerManager>().SetTriggerReadyToTask(false);
            box.SetActive(true);
            animator.SetBool("IsWalking", false);
            animator.SetBool("IsCarring", true);
            navMeshAgent.destination = (actualTask as Task.Transport).endingPlace.position;
            (actualTask as Task.Transport).endingPlace.GetComponent<TaskTriggerManager>().SetTriggerReadyToTask(true);

        }
        if(other.transform.position == (actualTask as Task.Transport).endingPlace.position)
        {
            resources.TryUse((actualTask as Task.Transport).resourceSO, 1);
            (actualTask as Task.Transport).endingPlace.GetComponent<TaskTriggerManager>().SetTriggerReadyToTask(false);
            box.SetActive(false);
            other.GetComponent<GameResourcesList>().Add((actualTask as Task.Transport).resourceSO, 1);
            animator.SetBool("IsCarring", false);
            state = State.Wait;
            actualTask = null;
        }
    }

}
