using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Task 
{
    public class Transport : Task
    {
        public Transform startingPlace;
        public GameResourceSO resourceSO;
        public Transform endingPlace;
    }
}
