using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TaskTriggerManager : MonoBehaviour
{
    [SerializeField] Collider taskTrigger;

    private void Awake()
    {
        taskTrigger.enabled = false;
    }

    public void SetTriggerReadyToTask(bool value)
    {
        taskTrigger.enabled = value;
    }
}
